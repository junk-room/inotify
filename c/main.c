#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/inotify.h>
#include <sys/types.h>
#include <unistd.h>  // read(2)

#define EVENT_SIZE (sizeof(struct inotify_event))
#define BUF_LEN    (1024 * (EVENT_SIZE + 16))

void inotify_events_loop(int fd);
int  inotify_read_events(int fd);

int line_num = 0;

int main(int argc, char *argv[], char *envp[])
{
	int fd = -1;
	int wd = -1;

	char *filename = NULL;

	if (argc != 2) {
		fprintf(stderr, "Usage:%s filepath\n", argv[0]);
		return 1;
	} 

	filename = argv[1];

	if ((fd = inotify_init()) == -1 ) {
		perror( "inotify_init" );
		return 1;
	}

	wd = inotify_add_watch(fd, argv[1], IN_MODIFY | IN_CREATE | IN_DELETE);

	inotify_events_loop(fd);

	inotify_rm_watch(fd, wd);
	close(fd);

	return 0;
}

void inotify_events_loop(int fd)
{
	int count = -1;

	while (1) {
		count = inotify_read_events(fd);
		printf("events count[%d]\n", count);
	}
}

int inotify_read_events(int fd)
{
	int  count = 0;
	char buffer[BUF_LEN];
	int  length = 0;

	if ((length = read(fd, buffer, BUF_LEN)) < 0) {
		return count;
	}

	for (int i = 0; i < length; i += EVENT_SIZE + event->len) {
		struct inotify_event *event = (struct inotify_event *) &buffer[i];

		/* 必要なイベントの処理 */
		if (event->mask & IN_CREATE) {
			printf("%d: %s was created.\n", line_num, event->name);
			line_num++;
		}
		if (event->mask & IN_MODIFY) {
			printf("%d: %s was modified.\n", line_num, event->name);
			line_num++;
		}

		count++;
	}

	return count;
}

// These are defined in /usr/include/x86_64-linux-gnu/sys/inotify.h

// struct inotify_event {
//         int      wd;              // Watch discriptor
//         uint32_t mask;            // Watch mask
//         uint32_t cookie;          // Cookie to synchronize two events
//         uint32_t len;             // Length (including NULs) of name
//         char     name __flexarr;  // Name
// }

// IN_ACCESS        00000000000000000000000000000001
// IN_MODIFY        00000000000000000000000000000010
// IN_ATTRIB        00000000000000000000000000000100
// IN_CLOSE_WRITE   00000000000000000000000000001000
// IN_CLOSE_NOWRITE 00000000000000000000000000010000
// IN_CLOSE         00000000000000000000000000011000
// ...
